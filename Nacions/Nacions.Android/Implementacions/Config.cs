﻿// é igual a meter o Add reference
// Ao mesmo tempo está a fazer o pré-compilar, ou seja, cria o dll
[assembly: Xamarin.Forms.Dependency(typeof(Nacions.Droid.Implementations.Config))]

namespace Nacions.Droid.Implementations
{
    using Interfaces;
    using SQLite.Net.Interop;

    public class Config : IConfig
    {
        private string directoryDB;

        private ISQLitePlatform platform;

        public string DirectoryDB
        {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    // Ir buscar a basta onde se cria a base de dados
                    directoryDB = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }

                return directoryDB;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    // Ir buscar a plataforma
                    platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                }

                return platform;

            }
        }
    }
}
