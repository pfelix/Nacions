﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nacions.Models
{
    public class Border
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
