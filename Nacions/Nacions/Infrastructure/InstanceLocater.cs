﻿using System;
using System.Collections.Generic;
using System.Text;
using Nacions.ViewModels;

namespace Nacions.Infrastructure
{
    public class InstanceLocater
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        #region Constructores

        public InstanceLocater()
        {
            this.Main = new MainViewModel();
        }


        #endregion

    }
}
