﻿using System;
using System.Collections.Generic;
using System.Text;
using Nacions.Models;
using Nacions.Views;

namespace Nacions.ViewModels
{
    public class MainViewModel
    {
        #region ViewModels

        public LoginViewModel Login { get; set; }
        public CountriesViewModel Countries { get; set; }
        public CountryViewModel Country { get; set; }

        #endregion

        #region Properties

        public List<Country> CountriesList { get; set; }
        public TokenResponse Token { get; set; }

        #endregion

        #region Constructures

        public MainViewModel()
        {
            instance = this; // aqui estamos a dizer que esta instancia sou eu
            this.Login = new LoginViewModel();
        }

        #endregion

        // Desing Patarn
        #region Singleton
        // É necessário criar propriedade e metodo static para se conseguir chamar na LoginViewModel
        private static MainViewModel instance;

        // Vai mandar a instancia para o exterior
        public static MainViewModel GetInstance()
        {
            // Verifica se a MainViewModel já foi criada
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }

        #endregion
    }
}
