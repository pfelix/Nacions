﻿namespace Nacions.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Views;
    using System.Windows.Input;
    using Xamarin.Forms;

    /// <summary>
    /// Classe para consegue fazer o clik no pais, para conseguir utilizar a opção do Commando
    /// </summary>
    public class CountryItemViewModel : Country
    {
        public ICommand SelectCountryCommand
        {
            get { return new RelayCommand(SelectCountry); }
        }

        private async void SelectCountry()
        {
            // Passa no contrutor o Country que foi selecionado
            MainViewModel.GetInstance().Country = new CountryViewModel(this);

            // Mostra a CountryTabbedPage que depois tem todas as páginas do Country
            await Application.Current.MainPage.Navigation.PushAsync(new CountryTabbedPage());
        }
    }
}
