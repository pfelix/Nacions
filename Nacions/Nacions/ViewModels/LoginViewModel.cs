﻿using Nacions.Services;

namespace Nacions.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Nacions.Views;
    using System.Windows.Input;
    using Xamarin.Forms;

    // Craido uma classe genérica para implementar os delegates
    public class LoginViewModel : BaseViewModel
    {
        #region Attributes

        private bool _isRunnig;
        private bool _isRemembered;
        private bool _isEnabled;
        private string _email;
        private string _password;
        private ApiService _apiService;

        #endregion

        #region Properties

        public bool IsRunnig
        {
            get { return _isRunnig; }
            set { SetValue(ref this._isRunnig, value); }
        }

        public bool IsRemembered
        {
            get { return _isRemembered; }
            set { SetValue(ref this._isRemembered, value);}
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetValue(ref this._isEnabled, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetValue(ref this._email, value);}
        }

        public string Password
        {
            get { return _password; }
            set { SetValue(ref this._password, value); }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Login); }
        }

        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter email, please.",
                    "Ok");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter password, please.",
                    "Ok");
                return;
            }

            this.IsRunnig = true;
            this.IsEnabled = false;

            //Autenticação usando o Token (API)

            #region Testar conecção

            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                this.IsRunnig = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Ok");
                return;
            }

            #endregion

            //#region Token

            //var token = await this._apiService.GetToken("http://nacionsapipaulo.azurewebsites.net/", this.Email,
            //    this.Password);

            //if (token == null)
            //{
            //    this.IsRunnig = false;
            //    this.IsEnabled = true;
            //    await Application.Current.MainPage.DisplayAlert("Error", "Something was wrong, please try later.", "Ok");
            //    return;
            //}

            //// Token vem vazio (Exp. login inválido)
            //if (string.IsNullOrEmpty(token.AccessToken))
            //{
            //    this.IsRunnig = false;
            //    this.IsEnabled = true;
            //    await Application.Current.MainPage.DisplayAlert("Error", token.ErrorDescription, "Ok");
            //    return;
            //}

            //// Atribuir à propriedade que está no MainViewModel o token para ser acedido no resto da aplicação
            var mainViewModel = MainViewModel.GetInstance();
            //mainViewModel.Token = token;

            //#endregion

            //Autenticação Sem o Token
            if (this.Email != "paulo@hotmail.com" || this.Password != "1234")
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Email or password incorrect!",
                    "Ok");

                // limpar a password
                this.Password = string.Empty;

                this.IsRunnig = false;
                this.IsEnabled = true;

                return;
            }

            await Application.Current.MainPage.DisplayAlert(
                "Boa",
                "Entramos !!!!",
                "Ok");

            this.IsRunnig = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

            // Instanciar o CountriesViesModel
            // Só é possivel porque foi criado um Singleton na MaindViewModel
            mainViewModel.Countries = new CountriesViewModel();
            // Passar para outra página
            await Application.Current.MainPage.Navigation.PushAsync(new CountriesPage());
        }

        public ICommand RegisterCommand
        {
            get { return new RelayCommand(Register);}
        }

        private async void Register()
        {
            await Application.Current.MainPage.DisplayAlert(
                "Registo",
                "Registo",
                "Ok");
        }

        public ICommand LoginFacebookCommand
        {
            get { return new RelayCommand(LoginFacebook);}
        }

        private async void LoginFacebook()
        {
            await Application.Current.MainPage.DisplayAlert(
                "Login Facebook",
                "Not done, yet!",
                "Ok");
        }

        #endregion

        #region Constructores

        public LoginViewModel()
        {
            this._apiService = new ApiService();
            this.IsRemembered = true;
            this._isEnabled = true;
            this.Email = "paulo@hotmail.com";
            this.Password = "1234";
        }

        #endregion
    }

    // Implementado os delegates nesta classe
    //// Para usar o delegate primeiro fazer herança
    //public class LoginViewModel:INotifyPropertyChanged
    //{
    //    // Implementar o metodo que vem da interface que é um evento de quando a propriedade é alterada
    //    #region Events

    //    public event PropertyChangedEventHandler PropertyChanged;

    //    #endregion

    //    #region Attributes

    //    private bool _isRunnig;
    //    private bool _isToggled;

    //    #endregion

    //    #region Properties

    //    public bool IsRunnig
    //    {
    //        get { return _isRunnig; }
    //        set
    //        {
    //            if (this._isRunnig != value)
    //            {
    //                this._isRunnig = value;

    //                // Isto chama-se delegate
    //                // Para alterar do lado do xaml
    //                // [Nome do evento ]?.Invoke() - para invocar o evento
    //                // O nameof garante que é mesmo o nome da Propriedade
    //                PropertyChanged?.Invoke(
    //                    this,
    //                    new PropertyChangedEventArgs(nameof(IsRunnig)));
    //            }
    //        }
    //    }

    //    public bool IsToggled
    //    {
    //        get { return _isToggled; }
    //        set
    //        {
    //            if (this._isToggled != value)
    //            {
    //                this._isToggled = value;

    //                PropertyChanged?.Invoke(
    //                    this,
    //                    new PropertyChangedEventArgs(nameof(IsToggled)));
    //            }
    //        }
    //    }

    //    #endregion

    //    #region Constructores

    //    public LoginViewModel()
    //    {
    //        this.IsRunnig = true;
    //        this.IsToggled = true;
    //    }

    //    #endregion
    //}
}
