﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Nacions.Models;

namespace Nacions.ViewModels
{
    public class CountryViewModel:BaseViewModel
    {
        #region Attributes

        private ObservableCollection<Border> _borders;
        private ObservableCollection<Currency> _currencies;
        private ObservableCollection<Language> _languages;

        #endregion

        #region Properties

        public Country Country { get; set; }

        public ObservableCollection<Border> Borders
        {
            get { return this._borders; }
            set { SetValue(ref this._borders, value);}
        }

        // No lado da View conseguimos aceder diretamente aos dados pela propriedade Country
        // No entanto é recumendado fazer desta forma
        public ObservableCollection<Currency> Currencies
        {
            get { return this._currencies; }
            set { SetValue(ref this._currencies, value); }
        }

        public ObservableCollection<Language> Languages
        {
            get { return this._languages; }
            set { SetValue(ref this._languages, value); }
        }

        #endregion

        #region Constructor

        public CountryViewModel(Country country)
        {
            this.Country = country;
            this.Currencies = new ObservableCollection<Currency>(this.Country.Currencies);
            this.Languages = new ObservableCollection<Language>(this.Country.Languages);

            LoadBorders();
        }

        #endregion

        private void LoadBorders()
        {
            Borders = new ObservableCollection<Border>();

            foreach (var Border in Country.Borders)
            {
                // Puxar os paises que são igual
                var country = MainViewModel.GetInstance().CountriesList.FirstOrDefault(c => c.Alpha3Code == Border);

                if (country != null)
                {
                    // Carrega com o nome e o código
                    this.Borders.Add(new Border
                    {
                        Code = country.Alpha3Code,
                        Name = country.Name
                    });
                }
            }
        }
    }
}
