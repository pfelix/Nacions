﻿namespace Nacions.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Models;
    using Newtonsoft.Json;
    using Plugin.Connectivity;

    public class ApiService
    {
        // Necessário Nuget Xam.Plugin.Connectivity
        public async Task<Response> CheckConnection()
        {
            try
            {
                // Verifica se existe ligação á internet
                if (!CrossConnectivity.Current.IsConnected)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = "Please see your Internet Config."
                    };
                }

                // Verifica se consegue aceder á internet
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

                if (!isReachable)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = "Please see your Internet Connection."
                    };
                }

                return new Response
                {
                    IsSucess = true,
                    Message = "Internet Connection Ok."
                };

            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        public async Task<TokenResponse> GetToken(
            string urlBase,
            string userName,
            string password)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                // Fazer o POST
                // StringContent - Criar a string e com o tipo de encriptação para enviar apela API
                var response = await client.PostAsync("Token",
                    new StringContent(string.Format(
                            "grant_type=password&username={0}&password={1}", userName, password),
                            Encoding.UTF8,
                            "application/x-www-form-urlencoded"));

                // Guardar resultado em Json
                var resultJSON = await response.Content.ReadAsStringAsync();
                
                // Converter o resultado no tipo TokenResponse
                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJSON);

                return result;

            }
            catch (Exception e)
            {
                return null;
            }

        }


        public async Task<Response> GetList<T>(
            string urlBase,
            string servicePrefix,
            string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                var response = await client.GetAsync(url);

                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }
    }
}
