﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nacions.Backend.Startup))]
namespace Nacions.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
